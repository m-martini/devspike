package it.webscience.devspike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevspikeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DevspikeApplication.class, args);
	}

}
